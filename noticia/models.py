from django.db import models

# Create your models here.

class Categoria(models.Model):
	nombre = models.CharField(max_length=144)
	creado = models.DateTimeField(auto_now_add=True)
	actualizado = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.nombre


class Posteo(models.Model):
	titulo = models.CharField(max_length=144)
	contenido = models.TextField()
	fecha_publicacion = models.DateField()
	estado = models.BooleanField(default=False)
	orden = models.IntegerField(default=0)
	categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
	creado = models.DateTimeField(auto_now_add=True)
	actualizado = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.titulo
