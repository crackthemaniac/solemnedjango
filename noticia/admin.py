from django.contrib import admin
from noticia.models import Categoria, Posteo

# Register your models here.

@admin.register(Categoria)
class CategoriaAdmin(admin.ModelAdmin):
	pass

@admin.register(Posteo)
class PosteoAdmin(admin.ModelAdmin):
	list_display = (
		'titulo',
		'contenido',
		'fecha_publicacion',
		'estado',
		'orden',
		'categoria',
		'creado',
		'actualizado',
	)