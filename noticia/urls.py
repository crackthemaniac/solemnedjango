from django.urls import path
from noticia import views

urlpatterns = [
    path('', views.inicio, name='inicio'),
    path('detalle_noticia/', views.detalle, name='detalle_noticia'),
]
