from django.shortcuts import render
from noticia.models import Posteo

# Create your views here.


def inicio(request):
	template_nombre = 'index.html'
	#data = {}
	#SELECT * FROM app WHERE status = 1

	#data['posts'] = Posteo.objects.filter(status=True)
	return render(request, template_nombre)


def detalle(request):
	template_nombre= 'noticia_detalle.html'
	return render(request, template_name, {})

